import { Component, OnInit, Output , EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  empDetails = []
  employee = []

  name = ''
  location = ''
  email = ''
  mobile = ''

  constructor(private service: EmployeeService,private router: Router) { }

  ngOnInit() {
    this.getDetails()
  }

  getDetails(){
      this.service.getEmployeeDetails().subscribe(response => {
        if(response['status'] === 'success'){
          this.empDetails = response['data']
        }
    })
  }

  editEmployee(id : number){
    this.service.updateEmployee(id , this.name , this.location , this.email, this.mobile)
  }

  deleteEmployee(id: number){
    this.service.deleteEmployee(id).subscribe( response => {
      if(response['status']==='success'){
        this.getDetails()
      }
    })
  }

  getSelectedEmpDetails(id : number){
    this.service.getSelectedEmployeeDetails(id).subscribe(response => {
      if(response['status']==='success'){
        this.employee = response['data']
        this.router.navigate(['/employee-details/'+id])
      }
    })
  }
}
