import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {
    
  myForm = new FormGroup({
    name: new FormControl(),
    location: new FormControl(),
    email: new FormControl(),
    mobile: new FormControl()
  })

  constructor() {}

  ngOnInit() {
  }

}
